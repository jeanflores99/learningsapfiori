sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageBox) {
        "use strict";

        return Controller.extend("sap.btp.helloworld.controller.View1", {
            onInit: function () {

            },
            onCustomPress: function () {
                //debugger;

                
                MessageBox.alert("Esto es una aleta");
            }
        });
    });
